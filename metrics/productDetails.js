var mwsController = require('../controllers/mwsController')
var Channel = require('../models/channels')
var dataController = require('../controllers/data')

var getSellerConfig = function (data) {
  return new Promise(function (resolve, reject) {
    Channel.findOne({'SellerId': data.sellerId}, function (err, sConfig) {
      if (err) {
        reject(new Error(err)); return
      }
      data.config = sConfig
      resolve(data)
    })
  })
}

var getRequestObject = function (data) {
  return new Promise(function (resolve, reject) {
    if (!data.asin || !data.sConfig) {
      reject(new Error('Unable to get request object'))
      return
    }
    var asins = []
    asins['IdList.Id.' + 1] = data.asin
    data.requestObject = {
      config: data.sConfig,
      data: {
        MarketplaceId: data.sConfig.MarketplaceId,
        IdType: 'ASIN',
        IdList: asins
      }
    }
    resolve(data)
  })
}

var loadProductDetails = function (data) {
  return new Promise(function (resolve, reject) {
    mwsController.getMatchingProducts(data.requestObject, function (products) {
      if (!products) {
        reject(new Error('Unable to load product details'))
        return
      }
      resolve(products)
    })
  })
}

var saveProductDetails = function (products) {
  return new Promise(function (resolve, reject) {
    dataController.save('PRODUCT_DETAILS', products).then(function () {
      resolve('Successfull saved product details')
    }).catch(function (err) {
      reject(new Error(err))
    })
  })
}

var getProductDetails = function (data) {
  return new Promise(function (resolve, reject) {
    if (!data || !data.asin || !data.sellerId) {
      reject(new Error('Invalid argument')); return
    }

    getSellerConfig(data)
      .then(getRequestObject)
      .then(loadProductDetails)
      .then(saveProductDetails)
      .then(function (data) {
        resolve('Got product details')
      })
      .catch(function (error) {
        console.error(error)
        reject(new Error(error))
      })
  })
}

module.exports = {
  getProductDetails: getProductDetails,
  GetProductDetails: function (data, config, cb) {
    var asins = []
    asins['IdList.Id.' + 1] = data
    var request = {}
    request.config = config
    request.data = {
      MarketplaceId: config.MarketplaceId,
      IdType: 'ASIN',
      IdList: asins
    }
    mwsController.getMatchingProducts(request, function (products) {
      var Productdetails = getFullProductDetails(products)
      cb(null, Productdetails)
    }, function (failure) {
      console.log(failure)
      cb(failure, {})
    })
  }
}


function getSmallImage(product){
    if(!product.AttributeSets){ return ''};
    return product.AttributeSets['ns2:ItemAttributes']['ns2:SmallImage']['ns2:URL'];
}

function getDept(product){
    return product.AttributeSets['ns2:ItemAttributes']['ns2:Department'] ? product.AttributeSets['ns2:ItemAttributes']['ns2:Department'] : 0;
}

function getQuantity(product){
    return product.AttributeSets['ns2:ItemAttributes']['ns2:PackageQuantity'] ?product.AttributeSets['ns2:ItemAttributes']['ns2:PackageQuantity'] : 0;
}

function getSalesRanking(product){
    if(!product.SalesRankings){ return 0;}
    if(!product.SalesRankings.SalesRank){ return 0;}
    return product.SalesRankings.SalesRank ? product.SalesRankings.SalesRank : 0;
}


function getTitle(product){
    return product.AttributeSets['ns2:ItemAttributes']['ns2:Title'] ? product.AttributeSets['ns2:ItemAttributes']['ns2:Title'] : '';
}

function getTitleLength(product){
    return getTitle(product).length;
}


function getProductGroup(product){
    return product.AttributeSets['ns2:ItemAttributes']['ns2:ProductGroup'] ? product.AttributeSets['ns2:ItemAttributes']['ns2:ProductGroup'] : 0;
}

function getBrand(product){
      return product.AttributeSets['ns2:ItemAttributes']['ns2:Brand'] ? product.AttributeSets['ns2:ItemAttributes']['ns2:Brand'] : '';
}

function getBulletpointLength(product){
    return product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'] ? product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'].length : 0;
}

function getBulletpoints(product){
    return product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'] ? product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'] : [];
}

function getDescription(product){
    return product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'] ? product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'] : '';
}

function getDescriptionLength(product){
    return product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'] ? product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'].length : 0;
}

function getProduct(data){
    if(!data){ return undefined;}
    if(!data.GetMatchingProductForIdResult){ return undefined;}
    if(!data.GetMatchingProductForIdResult.Products){ return undefined;}
    if(!data.GetMatchingProductForIdResult.Products.Product){ return undefined;}
    return data.GetMatchingProductForIdResult.Products.Product;
}

function getFullProductDetails (data) {
  var newProducts = {}
  newProducts = {
    
    imgurl: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:SmallImage']['ns2:URL'],
    dept: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Department'] ? data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Department'] : 0,
    quantity: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:PackageQuantity'] ? data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:PackageQuantity'] : 0,
    salesRanking: data.GetMatchingProductForIdResult.Products.Product.SalesRankings.SalesRank ? data.GetMatchingProductForIdResult.Products.Product.SalesRankings.SalesRank : 0,
    titleLength: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Title'].length ? data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Title'].length : 0,
    title: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Title'] ? data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Title'] : 0,
    productGroup: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:ProductGroup'] ? data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:ProductGroup'] : 0,
    brand: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Brand'] ? data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Brand'] : 0,
    bulletPointsLength: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes'] .length ? data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes'].length : 0,
    bulletPoints: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'] ? data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'] : 0,
    description: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'] ? data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes']['ns2:Feature'] : '',
    descriptionLength: data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes'].length ? data.GetMatchingProductForIdResult.Products.Product.AttributeSets['ns2:ItemAttributes'].length : 0
  }
  return newProducts
}
