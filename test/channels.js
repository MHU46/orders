process.env.NODE_ENV = 'test'

const mongoose = require('mongoose')
const chai = require('chai')
const chaiHttp = require('chai-http')
const resp = require('../constants/response')
const assert = require('chai').assert;

var should = chai.should()
const Channels = require('../models/channels')

var server = require('../app')
chai.use(chaiHttp)

describe('Channels', function () {
  beforeEach(function (done) {
    Channels.remove({}, function (err) {
      done()
    })
	    })
  describe('/channels/new', function () {
    it('should fail with sellerId missing message', function (done) {
      let data = { sellerId: undefined }
      chai.request(server)
			    .post('/channels/new')
			    .send(data)
			    .end(function (err, res) {
				    res.should.have.status(200)
				    res.body.should.have.property('status')
				    res.body.should.have.property('status').eql(resp.EMPTY_SELLERID.status)
				    res.body.should.have.property('message').eql(resp.EMPTY_SELLERID.message)
				    done()
        })
    })
    it('should fail with sellerId missing message',function (done) {
      let data = { sellerId: '' };
      chai.request(server)
			    .post('/channels/new')
			    .send(data)
			    .end(function (err, res) {
				    res.should.have.status(200)
				    res.body.should.have.property('status')
				    res.body.should.have.property('status').eql(resp.EMPTY_SELLERID.status)
				    res.body.should.have.property('message').eql(resp.EMPTY_SELLERID.message)
				    done()
        })
    })      
    it('should fail with MWSAuthorizationToken missing message', function (done) {
      let data = {
        MWSAuthorizationToken: ''
      };
      chai.request(server)
			    .post('/channels/new')
			    .send(data)
			    .end(function (err, res) {
				    res.should.have.status(200)
				    res.body.should.have.property('status')
				    res.body.should.have.property('status').eql(resp.EMPTY_SELLERID.status)
				    res.body.should.have.property('message').eql(resp.EMPTY_SELLERID.message)
				    done()
        })
    })
	})
  
})
