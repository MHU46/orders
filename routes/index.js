var express = require('express');
var router = express.Router();
const expressJwt = require('express-jwt');
var orderController = require('../controllers/orders');
var channelController = require('../controllers/channels');
var productController = require('../controllers/products');

//orders crud
router.post('/orders', orderController.Orders);
router.post('/nextOdr', orderController.OrdersByNextToken);
//channels crud
router.post('/channels/new', channelController.create);
router.get('/channels/:id', channelController.getBySellerId);
router.delete('/channels/:id', channelController.deleteChannel);
router.put('/update/:id', channelController.updateChannel);
//product crud
router.post('/products/new', productController.createProduct);
router.delete('/products/:id', productController.deleteProductByAsin);
router.get('/products/:Id', productController.getProduct);
router.put('/update/:id', productController.updateProduct);

module.exports = router;
