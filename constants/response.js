'use strict'

module.exports = Object.freeze({
    INVALID_REQUEST: {'status': 'failed', 'message': 'Invalid request'},
    ADDCHANNEL_TO_CONTINUE:{'status':'failed','message':'Please Enter Your credentials to continue'},
    CANNOT_SAVE:{'status':'failed','Message':'CANNOT SAVE MONGO ERROR'},
    CANNOT_FIND_SELLER:{'status':'failed','message':'cannot find seller Mongo error'},
    UNABLE_TO_ADDCHANNEL: { 'status': 'failed','message': 'Unable to add seller Mongo error'},
    INVALID_SELLERID:{'status': 'failed', 'message': 'UNDEFINED SELLERID Provided'},
    EMPTY_SELLERID:{'status': 'failed', 'message': 'EMPTY SELLERID Provided'},
    INVALID_MarketplaceId:{'status': 'failed', 'message': 'INVALID MARKETPLACEID Provided'},
    EMPTY_MarketplaceId:{'status': 'failed', 'message': 'EMPTY MARKETPLACEID Provided'},
    INVALID_Slug:{'status': 'failed', 'message': 'INVALID Slug Provided'},
    EMPTY_Slug:{'status': 'failed', 'message': 'EMPTY Slug Provided'},
    EMPTY_ACCESSKEY: {'status':'failed','message':'AWS AccesskeyId is missing'},
    EMPTY_SECRETKEY: {'status':'failed','message':'SecretKey is missing'},
    EMPTY_MWSTOKEN: { 'status': 'failed','message': 'MWSAuthorization is missing'},
    INVALID_ASIN:{'status': 'failed', 'message': 'UNDEFINED ASIN Provided'},
    UNABLE_TO_SIGNUP: {'status': 'failed', 'message': 'unable to signup'},
    ADDCHANNEL_TO_CONTINUE:{'status':'failed','message':'Please Enter Your credentials to continue'},
    ALREADY_REGISTERED_SELLERID:{'status':'failed','message':'Already user registered with same sellerId'},
})
