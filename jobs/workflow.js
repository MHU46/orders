var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var WorkflowSchema = new Schema({
    name: {
        type: String,
        index: true
    },
    joblist: {
        type: String
    }
});

WorkflowSchema.methods.getNextJob = function (jobName, cb) {
    if (this.joblist == undefined || this.joblist == '') { return undefined; }
    if (jobName == undefined || jobName == '') { return undefined; }
    var jobs = this.joblist.split(',');
    var index = jobs.indexOf(jobName.toLowerCase());
    if (index >= 0 && (index + 1) < jobs.length) {
        return jobs[index + 1];
    }
    return undefined;
};

module.exports = mongoose.model('Workflow', WorkflowSchema);