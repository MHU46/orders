var kue = require('kue');
var queue = kue.createQueue();
var mongoose = require('mongoose');
var cron = require('node-cron');
var Workflow = require('./workflow');

function getDBString () {
    return 'mongodb://127.0.0.1/sellergyan-production';
}

function connect () {
    var dbString = getDBString();
    mongoose.connect(dbString, {useMongoClient: true});
}

function init () {
    console.log('connection to mongo successful');
    initWorkflow();
    initCronJob();
}

connect();
mongoose.connection.on('close', connect);
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
mongoose.connection.on('open', init);

function initWorkflow(){
    var wfName = 'products';
    var wfList = 'productdetails';
    Workflow.remove({},function(){
        var wf = new Workflow({
	    name: wfName,
	    joblist: wfList
        });
        wf.save();     
    });

}
function initCronJob(){
    cron.schedule('* 1 * * *',function(){
	    console.log('Cronjob starting products job job');
        queue.create('products',{}).save();
    });
}

