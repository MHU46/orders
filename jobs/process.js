var kue = require('kue');
var config = require('config');
var mongoose = require('mongoose');
var processName = require('../constants/job_names.js');
var Product = require('../models/products');
var mws = require('mws-nodejs').mws;
var Seller = require('../models/channels');
var queue = kue.createQueue();
var productDetails = require('../metrics/productDetails')


function getDBString () {
    return 'mongodb://127.0.0.1/sellergyan-production';
}
function connect () {
    console.log('Connecting to database..');
    var dbString = getDBString();
    mongoose.connect(dbString, {useMongoClient: true});
}
function init () {
    console.log('connection to mongo successful');
}

connect();
mongoose.connection.on('close', connect);
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
mongoose.connection.on('open', init);

function productDet (product) {
    var details = {};
    details = {
        product: product
    };
    return details;
}

function log(loglevel,process,msg){
    console.log((new Date())+':'+process+': ('+loglevel+') :'+msg);
}

queue.process('productdetails', function (job, done) {
    log('log','productdetails','Starting process');
    var asin = job.data.product.product.asin;
    var iso = "ISODate"
    var time = job.data.product.product.createdAt;
    var IsoDate = iso.concat('('+'"'+time+'"'+')');
    var sconfig = job.data.product.sconfig;
    var sellerId = job.data.product.sconfig.SellerId;
    
        productDetails.GetProductDetails(asin, sconfig, function (error, productdetails) {
            if (error) {
                log('error','productdetails','Unable to get product details');
                done(new Error('Unable to get product details'));
                return;
            }
            var result = productDet(productdetails);
            Product.findOne({'asin': asin, 'createdAt': time, 'sellerId': sellerId}, function (err, product) {
                if (err) {
                    log('error','productdetails','Unable to find product');
                    done(new Error('Unable to find product'));
                    return;
                }
                log('log','productdetails','Updating existing product details');
                product.product = result;
                product.save(function () {
                    log('log','productdetails','Successfully updated existing product details');
                    if (job.data.workflow !== undefined) {
                    }
                    log('log','productdetails','Ending process');
                    done();
                });
            });
        }); 
});