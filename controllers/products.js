var Product =require('../models/products.js');
var Seller = require('../models/channels');
var jobController = require('./jobs');
var responses = require('../constants/response');

function ProductController () {
    
}

function getSellerConfig(sellerId,cb){
    Seller.findOne({'SellerId':sellerId}, function (err, seller) {
        if (err){
            console.log("Unable to get seller config, mongo error");
            console.error(err)
            cb(err,null)
            return
        }
        if(seller == null){
        cb(new Error("Sellerconfig empty"),null);
        return
        }
        cb(null,seller)
    })
}
var isValidArg = function(arg){
    if(arg == undefined || arg == '' || arg == null){
	return false;
    }
    return true;
}

// Add product
ProductController.prototype.createProduct = function (req, res) {

    var asin = req.body.asin;
    var sellerId = req.body.sellerId;
    var MarketplaceId = req.body.MarketplaceId;
    var Slug = req.body.Slug;
    if(!isValidArg(req.body.asin)){
    res.json(responses.INVALID_ASIN);return
    }
    if(!isValidArg(req.body.sellerId)){
    res.json(responses.INVALID_SELLERID);return
    }
    if(!isValidArg(req.body.MarketplaceId)){
    res.json(responses.INVALID_MarketplaceId);return
    }
    if(!isValidArg(req.body.Slug)){
    res.json(responses.INVALID_Slug);return
    }   

    getSellerConfig(req.body.sellerId,function(err,sconfig){
        if (err){
            console.error(err)
            res.json(responses.CANNOT_GET_SELLER_CONFIG)
            return
            }
        if(sconfig == null){
        console.error("Unable to get sellerconfig for sellerId:"+req.body.sellerId);
        res.json(responses.ADDCHANNEL_TO_CONTINUE)
        return
        }
        if (!sconfig){
            res.json(responses.ADDCHANNEL_TO_CONTINUE)
            return
        }
        else{
            var product = new Product(req.body)
            product.product={}
            product.save(function (err) {
                if (err) {
                    res.json(responses.CANNOT_SAVE)
                    return
                } else {
                    jobController.newproduct({product:product,sconfig:sconfig})
                    res.json(product)
                }
            })
        }
    })
}
// Get product
ProductController.prototype.getProduct = function (req, res) {
    if (req.params.Id == undefined || req.params.Id == '') {
      sendErrorResponse(res, 200, { 'message': 'Invalid Id provided'}); return
    }
    var data = {
      'asin': req.params.Id
    }
    Product.findOne(data, function (err, product) {
      if (err) { sendErrorResponse(res, 200, { 'message': 'Unable to find product' }); return }
      res.json(product)
    })
}
// Update product
ProductController.prototype.updateProduct = function (req, res) {
    var product = new Product(req.body)
    var upsertData = product.toObject()
    delete upsertData._id
    Product.update({'_id': req.params.Id}, upsertData, {upsert: true}, function (err, updatedProduct) {
      if (err) { sendErrorResponse(res, 200, { 'message': 'Unable to update product' }); return }
      res.json(updatedProduct)
    })
}
// Delete product
ProductController.prototype.deleteProductByAsin = function (req, res) {
    
    if (req.params.Id == undefined || req.params.Id == '') {
        Error(res, 200, { 'message': 'Invalid Id provided'}); return
    }
    var data = {
        '_id': req.params.Id
    }
    Product.remove(data, function (err, product) {
        if (product) {
        var response = {
            message: 'product successfully deleted'
        }
        res.send(response)
        } else {
        Error(res, 200, {'message': 'cannot delete product'})
        }
    })
}

var productController = new ProductController()
module.exports = productController