var kue = require('kue')
var jobs = kue.createQueue()
const jobNames=require('../constants/job_names')

function JobsController () {

}

function runJob(name,data,cb){
    var job  = jobs.create(name, data).priority('high').attempts(5).save();
    job.on('complete',cb);
}

JobsController.prototype.newproduct = function (product) {
    runJob('productdetails',{ workflow: "newproducts",product: product,sconfig:product.sconfig}, function(result){
      console.log("All Metrics Done")
      
    });
}

var jobsController = new JobsController()
module.exports = jobsController