var Channel = require('../models/channels')
const responses = require('../constants/response')

function ChannelController(){
}

var sendErrorResponse = function (res, status, error) {
    return res.status(status).json({
      error: error
    })
}

var isValidArg = function(arg){
    if(arg == undefined || arg == '' || arg == null){
	return false;
    }
    return true;
}
// Add channel start here
ChannelController.prototype.create = function (req, res) {

    var sellerId = req.body.SellerId;
    var MWSAuthorizationToken = req.body.MWSAuthorizationToken;
    var AWSAccessKeyId = req.body.AWSAccessKeyId;
    var SecretKey = req.body.SecretKey;
    var MarketplaceId=req.body.MarketplaceId;

    if(!isValidArg(sellerId)){
	console.log(Date.now()+": failed to create seller, sellerId is missing")
	res.json(responses.EMPTY_SELLERID);return
    }
    if(!isValidArg(MWSAuthorizationToken)){
	console.log(Date.now()+": failed to create seller, mwsauthtoken is missing")
	res.json(responses.EMPTY_MWSTOKEN);return
    }
    if(!isValidArg(AWSAccessKeyId)){
	console.log(Date.now()+": failed to create seller, awsaccesskeyid is missing")
	res.json(responses.EMPTY_ACCESSKEY);return
    }
    if(!isValidArg(SecretKey)){
	console.log(Date.now()+": failed to create seller, secretkey is missing")
	res.json(responses.EMPTY_SECRETKEY);return
    }
    if(!isValidArg(MarketplaceId)){
	console.log(Date.now()+": failed to create seller, marketplaceid is missing")
	res.json(responses.EMPTY_MarketplaceId);return
    }
    Channel.findOne({'SellerId':sellerId},function(err,channel){
        if (err){
          console.error(err)
          res.json(responses.CANNOT_FIND_SELLER)
          return
        }
        if (channel && channel.SellerId ==req.body.SellerId){
           res.json(responses.ALREADY_REGISTERED_SELLERID)
           return
        }
        else{
        var channel = new Channel({SellerId: sellerId, email: req.body.email})
        channel.MWSAuthorizationToken = req.body.MWSAuthorizationToken
        channel.AWSAccessKeyId = req.body.AWSAccessKeyId
        channel.SecretKey = req.body.SecretKey
        channel.MarketplaceId = req.body.MarketplaceId
	    channel.AmazonServicesURL = "mws.amazonservices.in"
        channel.save(function (err) {
            if (err){
                res.json(responses.UNABLE_TO_ADDCHANNEL);return
	    }
            res.json(channel)
      })
    }
  })
}
// Get channel through sellerid
ChannelController.prototype.getBySellerId = function (req, res) {
    var data = {
        'sellerId': req.params.sellerId
    }
    Channel.find(data,function(error,seller){
    if (error) {
    sendErrorResponse(res, 200, { 'message': 'Unable to get seller'})
    return
    }
    res.json(seller)
    })
}
//Update channel
ChannelController.prototype.updateChannel = function (req, res) {
    var channel = new Channel(req.body)
    var upsertData = channel.toObject()
    delete upsertData._id
    Channel.update({'_id': req.params.Id}, upsertData, {upsert: true}, function (err, updatedSeller) {
      if (err) { sendErrorResponse(res, 200, { 'message': 'Unable to update seller' }); return }
      res.json(upsertData)
    })
}
//delete channel
ChannelController.prototype.deleteChannel = function (req, res) {
    var data = {
      '_id': req.params.Id
    }
    Channel.remove(data, function (err, seller) {
      if (err) {
        sendErrorResponse(res, 200, {'message': 'cannot delete channel'})
        return
      } else {
        var response = {
          message: 'channel successfully deleted'
        }
        res.send(response)
      }
    })
}

var channelController = new ChannelController()
module.exports = channelController