var mws = require('mws-nodejs').mws

function MWS () {
}

function MWSRequestObject (config, request) {
  return {
    config: config,
    data: {
	    MarketplaceId: config['marketplaceId'],
	    IdType: 'ASIN',
	    IdList: request.asins
    }
  }
}

MWS.prototype.getMatchingProducts = function (request, success, failure) {
  mws.products
    .GetMatchingProductForId(
      request.config,
      request.data,
      false,
      function (err, products) {
				    if (err) { failure(err); return }
				    success(products)
      })
}
MWS.prototype.GetProductCategoriesForASIN = function (request, success, failure) {
  mws.products
    .GetProductCategoriesForASIN(
      request.config,
      request.data,
      false,
      function (err, products) {
				    if (err) { failure(err); return }
				    success(products)
      })
}
MWS.prototype.getLowestOfferListingsForASIN = function (request, success, failure) {
  mws.products.GetLowestOfferListingsForASIN(request.config,
					       request.data,
					       false,
					       function (err, repo) {
						   if (err)console.log("actual error\n\n",err); { failure(err); return }console.log("acrual repo\n\n",repo)
						   success(repo)
					       })
}

MWS.prototype.getOrders = function (request, success, failure) {
  mws.orders.ListOrders(request.config,
			  request.data,
			  false,
			  function (err, data) {
			      if (err) { failure(err); return }
			      success(data)
			  })
}

MWS.prototype.getOrderDetailsById = function (request, success, failure) {
  mws.orders.ListOrderItems(request.config,
			      request.data,
			      function (err, repo) {
				  if (err) { failure(err); return }
				  success(repo)
			      })
}

MWS.prototype.getReportList = function (request, success, failure) {
  mws.reports.GetReportList(request.config,
			     request.data,
			     false,
			     function (err, list) {
				 if (err) { failure(err); return }
				 success(list)
			     })
}

MWS.prototype.getReport = function (request, success, failure) {
  mws.reports.GetReport(request.config,
			  request.data,
			  function (err, report) {
			      if (err) { failure(err); return }
			      success(report)
			  })
}

var mwsController = new MWS()

module.exports = mwsController
