var mongoose = require('mongoose'),
Schema = mongoose.Schema

var AddProductSchema = new Schema({
asin: {
  type: String,
},
sellerId: {
  type: String,
},
MarketplaceId:{
  type: String,
},
sellerConfig:{
  type:Object,
},
Slug:{
    type: String
},
createdAt: {
  type: Date,
  default: Date.now()
}
})

module.exports = mongoose.model('AddProduct', AddProductSchema)
