var mongoose = require('mongoose'),
Schema = mongoose.Schema

var channelSchema = new Schema({
  SellerId:{
        type: String,
  },
  DeveloperAccountNumber:{
    type: String
  }, 
  AWSAccessKeyId:{
    type: String
  },
  SecretKey:{
    type: String
  },
  MWSAuthorizationToken:{
    type: String
  },
  AmazonServicesURL:{
    type:String
  },
  MarketplaceId:{
      type:String
    },
  email:{
    type: String
  }
})

module.exports = mongoose.model('channel', channelSchema)
