var mongoose = require('mongoose'),
Schema = mongoose.Schema

var OrderSchema = new Schema({

    asins: {
        type: Object,
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    sellerId: {
        type: String,
    }
})

module.exports = mongoose.model('Order', OrderSchema)
